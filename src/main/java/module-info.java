module cr.ac.una.mensajeria {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.logging;
    requires java.ws.rs;
    requires java.xml.bind;
    requires java.sql;
    requires java.base;
    requires com.jfoenix;
    requires cr.ac.una.gestorseguridad;
    requires java.rmi;

    opens cr.ac.una.mensajeria.controller to javafx.fxml,javafx.controls,com.jfoenix;
    exports cr.ac.una.mensajeria to javafx.graphics;
    exports cr.ac.una.mensajeria.model;
}
