/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.mensajeria.controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDrawer;
import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import java.rmi.RemoteException;
import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;

/**
 * FXML Controller class
 *
 * @author Ale
 */
public class ChatController extends Controller implements Initializable {

    @FXML
    private AnchorPane root;
    @FXML
    private JFXButton btnEnviar;
    @FXML
    private JFXDrawer drawerPane;
    @FXML
    private ScrollPane scrollChat;
    @FXML
    private VBox chatVBox;
    @FXML
    private ScrollPane scrollContactos;
    @FXML
    private VBox vBoxChat;
    @FXML
    private VBox vBoxContactos;
    @FXML
    private VBox vBoxContContactos;
    @FXML
    private VBox vboxIni;
    @FXML
    private TextField txtMensaje;
    
    private String empleado = "emp";
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        scrollChat.vvalueProperty().bind(chatVBox.heightProperty());
        
    }    

    @Override
    public void initialize() {
    }

    @FXML
    private void onActionBtnEnviar(ActionEvent event) {
        
        try {
            if(txtMensaje.getText().trim().equals(""))return;
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        txtMensaje.setText("");
        txtMensaje.requestFocus();
        
    }
    
    public void setEmpleado(String emp){
        this.empleado=emp;
        System.out.println(this.empleado);
        try {
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public boolean Actualizar(String emp,String msg) {
        Text text=new Text(msg);

        text.setFill(Color.WHITE);
        text.getStyleClass().add("msg");
        TextFlow tempFlow=new TextFlow();
        if(!this.empleado.equals(emp)){
            Text txtName=new Text(emp + "\n");
            txtName.getStyleClass().add("txtNombre");
            tempFlow.getChildren().add(txtName);
        }

        tempFlow.getChildren().add(text);
        tempFlow.setMaxWidth(200);

        TextFlow flow=new TextFlow(tempFlow);

        HBox hbox=new HBox(12);

        Circle img =new Circle(32,32,16);
        try{
            System.out.println(emp);
//            String path= new File(String.format("Para la posible imagen ", emp)).toURI().toString();
//            img.setFill(new ImagePattern(new Image(path)));
        }catch (Exception ex){
//            String path= new File("Para la posible img").toURI().toString();
//            img.setFill(new ImagePattern(new Image(path)));
        }

        img.getStyleClass().add("imageView");

        if(!this.empleado.equals(emp)){

            tempFlow.getStyleClass().add("tempFlowFlipped");
            flow.getStyleClass().add("textFlowFlipped");
            chatVBox.setAlignment(Pos.TOP_LEFT);
            hbox.setAlignment(Pos.CENTER_LEFT);
            hbox.getChildren().add(img);
            hbox.getChildren().add(flow);

        }else{
            text.setFill(Color.WHITE);
            tempFlow.getStyleClass().add("tempFlow");
            flow.getStyleClass().add("textFlow");
            hbox.setAlignment(Pos.BOTTOM_RIGHT);
            hbox.getChildren().add(flow);
            hbox.getChildren().add(img);
        }

        hbox.getStyleClass().add("hbox");
        Platform.runLater(() -> chatVBox.getChildren().addAll(hbox));

        return true;

    }
    
}
