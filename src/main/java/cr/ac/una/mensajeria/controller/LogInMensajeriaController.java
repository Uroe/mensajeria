/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.mensajeria.controller;

import com.jfoenix.controls.JFXButton;
import cr.ac.una.gestorseguridad.ws.GestorSeguridadWs;
import cr.ac.una.gestorseguridad.ws.GestorSeguridadWs_Service;
import cr.ac.una.gestorseguridad.ws.RolDto;
import cr.ac.una.gestorseguridad.ws.UsuarioDto;
import cr.ac.una.mensajeria.util.AppContext;
import cr.ac.una.mensajeria.util.FlowController;
import cr.ac.una.mensajeria.util.Mensaje;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Ale
 */
public class LogInMensajeriaController extends Controller implements Initializable {

    @FXML
    private AnchorPane root;
    @FXML
    private TextField txtUsuario;
    @FXML
    private PasswordField pwfContraseña;
    @FXML
    private JFXButton btnShow;
    @FXML
    private JFXButton btnHide;
    @FXML
    private Hyperlink hplOlvidoContra;
    @FXML
    private JFXButton btnIniciarSesion;
    @FXML
    private JFXButton btnRegistrar;
    
    private String password;
    /**

     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        btnHide.setVisible(false);
    }    

    @FXML
    private void onActionBtnShow(ActionEvent event) {
        password = pwfContraseña.getText();
        pwfContraseña.clear();
        pwfContraseña.setPromptText(password);
        btnShow.setVisible(false);
        btnShow.setDisable(true);
        btnHide.setVisible(true);
        btnHide.setDisable(false);
    }

    @FXML
    private void onActionBtnHide(ActionEvent event) {
        pwfContraseña.setText(password);
        pwfContraseña.setPromptText("Contraseña");
        btnHide.setVisible(false);
        btnHide.setDisable(true);
        btnShow.setVisible(true);
        btnShow.setDisable(false);
    }

    @FXML
    private void onActionBtnIniciarSesion(ActionEvent event) {
        try {

            if (txtUsuario.getText() == null || txtUsuario.getText().isEmpty()) {
                new Mensaje().showModal(Alert.AlertType.ERROR, "Validación de usuario", (Stage) btnIniciarSesion.getScene().getWindow(), "Es necesario digitar un usuario para ingresar al sistema.");
            } else if (pwfContraseña.getText() == null || pwfContraseña.getText().isEmpty()) {
                new Mensaje().showModal(Alert.AlertType.ERROR, "Validación de usuario", (Stage) btnIniciarSesion.getScene().getWindow(), "Es necesario digitar la clave para ingresar al sistema.");
            } else {
                GestorSeguridadWs_Service service = new GestorSeguridadWs_Service();
                GestorSeguridadWs gestor = service.getGestorSeguridadWsPort();
                UsuarioDto usuarioDto = gestor.validarUsuario(txtUsuario.getText(), pwfContraseña.getText());
                String token = usuarioDto.getToken();
                usuarioDto = gestor.getUsuario(usuarioDto.getUsuId());
                if (usuarioDto != null) {
                    for (RolDto role : usuarioDto.getRoles()) {
                        if (role.getRolNombre().equals("Normal")) {
                            RolDto rol = gestor.getRol(role.getRolId());
                            if (rol.getSistema().getSisNombre().equals("Mensajeria")) {
                                AppContext.getInstance().set("Usuario", usuarioDto);
                                AppContext.getInstance().set("Rol", rol);
                                AppContext.getInstance().set("Token", token);
                                if (getStage().getOwner() == null) {
                                    FlowController.getInstance().goMain();
                                }
                                getStage().close();
                            }
                        } else {
                            new Mensaje().showModal(Alert.AlertType.ERROR, "Ingreso", getStage(), "No tiene permiso para acceder"
                                    + " a esta aplicacion");
                        }
                    }
                } else {
                    new Mensaje().showModal(Alert.AlertType.ERROR, "Ingreso", getStage(),
                            "Sus credenciales no coinciden");
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(LogInMensajeriaController.class.getName()).log(Level.SEVERE, "Error ingresando.", ex);
        }
    }

    @FXML
    private void onActionBtnRegistrar(ActionEvent event) {
    }

    @Override
    public void initialize() {
    }
    
}
