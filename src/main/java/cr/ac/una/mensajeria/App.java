package cr.ac.una.mensajeria;

import cr.ac.una.mensajeria.util.FlowController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import javafx.scene.image.Image;

/**
 * JavaFX App
 */
public class App extends Application {

    private static Scene scene;

    @Override
    public void start(Stage stage) throws IOException {
        FlowController.getInstance().InitializeFlow(stage,null);
        stage.getIcons().add(new Image(App.class.getResourceAsStream("/cr/ac/una/mensajeria/resources/Usuario.png")));
        stage.setTitle("Mensajeria Instantanea");
//        FlowController.getInstance().goMain();
        FlowController.getInstance().goViewInWindow("LogInMensajeria");
    }

    public static void main(String[] args) {
        launch();
    }

}