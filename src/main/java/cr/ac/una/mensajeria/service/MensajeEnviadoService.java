/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.mensajeria.service;

import cr.ac.una.mensajeria.model.MensajeEnviadoDto;
import cr.ac.una.mensajeria.util.Request;
import cr.ac.una.mensajeria.util.Respuesta;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.GenericType;

/**
 *
 * @author Usuario
 */
public class MensajeEnviadoService {

    public Respuesta getMensajeEnviado(Long id) {
        try {
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("id", id);
            Request request = new Request("MensajeEnviadoController/mensajeenviado", "/{id}", parametros);
            request.get();

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }
            MensajeEnviadoDto msjEnviado = (MensajeEnviadoDto) request.readEntity(MensajeEnviadoDto.class);
            return new Respuesta(true, "", "", "MensajeEnviado", msjEnviado);
        } catch (Exception ex) {
            Logger.getLogger(MensajeEnviadoService.class.getName()).log(Level.SEVERE, "Error obteniendo el mensaje [" + id + "]", ex);
            return new Respuesta(false, "Error obteniendo el mensaje.", "getMensajeEnviado" + ex.getMessage());
        }
    }

    public Respuesta getMensajesEnviados(Long idEmisor) {
        try {
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("idEmisor", idEmisor);
            Request request = new Request("MensajeEnviadoController/mensajesenviados", "/{idEmisor}", parametros);
            request.get();

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }
            List<MensajeEnviadoDto> mensajes = (List<MensajeEnviadoDto>) request.readEntity(new GenericType<List<MensajeEnviadoDto>>() {
            });
            return new Respuesta(true, "", "", "MensajesEnviados", mensajes);
        } catch (Exception ex) {
            Logger.getLogger(MensajeEnviadoService.class.getName()).log(Level.SEVERE, "Error obteniendo los mensajes.", ex);
            return new Respuesta(false, "Error obteniendo los mensajes.", "getMensajesEnviados " + ex.getMessage());
        }
    }

    public Respuesta guardarMensajeEnviado(MensajeEnviadoDto msjEnviado) {
        try {
            Request request = new Request("MensajeEnviadoController/mensajeenviado");
            request.post(msjEnviado);

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }

            MensajeEnviadoDto mensajeEnviadoDto = (MensajeEnviadoDto) request.readEntity(MensajeEnviadoDto.class);
            return new Respuesta(true, "", "", "MensajeEnviado", mensajeEnviadoDto);
        } catch (Exception ex) {
            Logger.getLogger(MensajeEnviadoService.class.getName()).log(Level.SEVERE, "Error guardando el mensaje.", ex);
            return new Respuesta(false, "Error guardando el mensaje.", "guardarMensajeEnviado " + ex.getMessage());
        }
    }

    public Respuesta eliminarMensajeEnviado(Long id) {
        try {
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("id", id);
            Request request = new Request("MensajeEnviadoController/mensajeenviado", "/{id}", parametros);
            request.delete();

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }
            return new Respuesta(true, "", "");
        } catch (Exception ex) {
            Logger.getLogger(MensajeEnviadoService.class.getName()).log(Level.SEVERE, "Error eliminando el mensaje.", ex);
            return new Respuesta(false, "Error eliminando el mensaje.", "eliminarMensajeEnviado" + ex.getMessage());
        }
    }
}
