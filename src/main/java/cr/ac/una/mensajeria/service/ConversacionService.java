/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.mensajeria.service;

import cr.ac.una.mensajeria.model.ConversacionDto;
import cr.ac.una.mensajeria.util.Request;
import cr.ac.una.mensajeria.util.Respuesta;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.GenericType;

/**
 *
 * @author Usuario
 */
public class ConversacionService {

    public Respuesta getConversacion(Long id) {
        try {
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("id", id);
            Request request = new Request("ConversacionController/conversacion", "/{id}", parametros);
            request.get();

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }
            ConversacionDto conversacionDto = (ConversacionDto) request.readEntity(ConversacionDto.class);
            return new Respuesta(true, "", "", "Conversacion", conversacionDto);
        } catch (Exception ex) {
            Logger.getLogger(ConversacionService.class.getName()).log(Level.SEVERE, "Error obteniendo la conversacion [" + id + "]", ex);
            return new Respuesta(false, "Error obteniendo la conversacion.", "getConversacion" + ex.getMessage());
        }
    }

    public Respuesta getConversaciones(Long idUsuario) {
        try {
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("idUsuario", idUsuario);
            Request request = new Request("ConversacionController/conversaciones", "/{idUsuario}", parametros);
            request.get();

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }
            List<ConversacionDto> conversacionDtos = (List<ConversacionDto>) request.readEntity(new GenericType<List<ConversacionDto>>() {
            });
            return new Respuesta(true, "", "", "Conversaciones", conversacionDtos);
        } catch (Exception ex) {
            Logger.getLogger(ConversacionService.class.getName()).log(Level.SEVERE, "Error obteniendo conversaciones.", ex);
            return new Respuesta(false, "Error obteniendo conversaciones.", "getConversaciones " + ex.getMessage());
        }
    }

    public Respuesta guardarConversacion(ConversacionDto conversacion) {
        try {
            Request request = new Request("ConversacionController/conversacion");
            request.post(conversacion);

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }

            ConversacionDto conversacionDto = (ConversacionDto) request.readEntity(ConversacionDto.class);
            return new Respuesta(true, "", "", "Conversacion", conversacionDto);
        } catch (Exception ex) {
            Logger.getLogger(ConversacionService.class.getName()).log(Level.SEVERE, "Error guardando la conversacion.", ex);
            return new Respuesta(false, "Error guardando la conversacion.", "guardarConversacion " + ex.getMessage());
        }
    }

    public Respuesta eliminarConversacion(Long id) {
        try {
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("id", id);
            Request request = new Request("ConversacionController/conversacion", "/{id}", parametros);
            request.delete();

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }
            return new Respuesta(true, "", "");
        } catch (Exception ex) {
            Logger.getLogger(ConversacionService.class.getName()).log(Level.SEVERE, "Error eliminando la conversacion.", ex);
            return new Respuesta(false, "Error eliminando la conversacion.", "eliminarConversacion" + ex.getMessage());
        }
    }
}
