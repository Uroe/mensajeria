/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.mensajeria.service;

import cr.ac.una.mensajeria.model.MensajeRecibidoDto;
import cr.ac.una.mensajeria.util.Request;
import cr.ac.una.mensajeria.util.Respuesta;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.GenericType;

/**
 *
 * @author Usuario
 */
public class MensajeRecibidoService {

    public Respuesta getMensajeRecibido(Long id) {
        try {
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("id", id);
            Request request = new Request("MensajeRecibidoController/mensajerecibido", "/{id}", parametros);
            request.get();

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }
            MensajeRecibidoDto msjRecibido = (MensajeRecibidoDto) request.readEntity(MensajeRecibidoDto.class);
            return new Respuesta(true, "", "", "MensajeRecibido", msjRecibido);
        } catch (Exception ex) {
            Logger.getLogger(MensajeRecibidoService.class.getName()).log(Level.SEVERE, "Error obteniendo el mensaje [" + id + "]", ex);
            return new Respuesta(false, "Error obteniendo el mensaje.", "getMensajeRecibido" + ex.getMessage());
        }
    }

    public Respuesta getMensajesRecibidos(Long idReceptor) {
        try {
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("idReceptor", idReceptor);
            Request request = new Request("MensajeRecibidoController/mensajesrecibidos", "/{idReceptor}", parametros);
            request.get();

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }
            List<MensajeRecibidoDto> mensajes = (List<MensajeRecibidoDto>) request.readEntity(new GenericType<List<MensajeRecibidoDto>>() {
            });
            return new Respuesta(true, "", "", "MensajesRecibidos", mensajes);
        } catch (Exception ex) {
            Logger.getLogger(MensajeRecibidoService.class.getName()).log(Level.SEVERE, "Error obteniendo los mensajes.", ex);
            return new Respuesta(false, "Error obteniendo los mensajes.", "getMensajesRecibidos " + ex.getMessage());
        }
    }

    public Respuesta guardarMensajeRecibido(MensajeRecibidoDto msjRecibido) {
        try {
            Request request = new Request("MensajeRecibidoController/mensajerecibido");
            request.post(msjRecibido);

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }

            MensajeRecibidoDto mensajeRecibidoDto = (MensajeRecibidoDto) request.readEntity(MensajeRecibidoDto.class);
            return new Respuesta(true, "", "", "MensajeRecibido", mensajeRecibidoDto);
        } catch (Exception ex) {
            Logger.getLogger(MensajeRecibidoService.class.getName()).log(Level.SEVERE, "Error guardando el mensaje.", ex);
            return new Respuesta(false, "Error guardando el mensaje.", "guardarMensajeRecibido " + ex.getMessage());
        }
    }

    public Respuesta eliminarMensajeRecibido(Long id) {
        try {
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("id", id);
            Request request = new Request("MensajeRecibidoController/mensajerecibido", "/{id}", parametros);
            request.delete();

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }
            return new Respuesta(true, "", "");
        } catch (Exception ex) {
            Logger.getLogger(MensajeRecibidoService.class.getName()).log(Level.SEVERE, "Error eliminando el mensaje.", ex);
            return new Respuesta(false, "Error eliminando el mensaje.", "eliminarMensajeRecibido" + ex.getMessage());
        }
    }
}
