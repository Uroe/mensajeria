/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.mensajeria.model;

import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author JosueNG
 */
public class MensajeEnviadoDto {
    
    public SimpleStringProperty id;
    public SimpleStringProperty idEmisor;    
    public SimpleStringProperty mensaje;
    public ConversacionDto conversacion;
    
    private Boolean modificado;

    public MensajeEnviadoDto(){
        this.modificado = false;
        this.id = new SimpleStringProperty();
        this.idEmisor = new SimpleStringProperty();
        this.mensaje = new SimpleStringProperty();
        conversacion = new ConversacionDto();
    }

    public Long getId() {
         if(id.get()!=null && !id.get().isEmpty())
            return Long.valueOf(id.get());
        else
            return null;
    }

    public void setId(Long id) {
        this.id.set(id.toString());
    }

    public Long getIdEmisor() {
         if(idEmisor.get()!=null && !idEmisor.get().isEmpty())
            return Long.valueOf(idEmisor.get());
        else
            return null;
    }

    public void setIdEmisor(Long idEmisor) {
        this.idEmisor.set(idEmisor.toString());
    }

    public String getMensaje() {
        return mensaje.get();
    }

    public void setMensaje(String mensaje) {
        this.mensaje.set(mensaje);
    }

    public Boolean getModificado() {
        return modificado;
    }

    public void setModificado(Boolean modificado) {
        this.modificado = modificado;
    }

    public ConversacionDto getConversacion() {
        return conversacion;
    }

    public void setConversacion(ConversacionDto conversacion) {
        this.conversacion = conversacion;
    }

    @Override
    public String toString() {
        return "MensajesEnviadosDto{" + "id=" + id + ", idEmisor=" + idEmisor + ", mensaje=" + mensaje + '}';
    }
      
}
