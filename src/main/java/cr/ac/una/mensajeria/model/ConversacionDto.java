/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.mensajeria.model;

import java.util.List;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;

/**
 *
 * @author Ale
 */
public class ConversacionDto {
    
    public SimpleStringProperty conId;
    public SimpleStringProperty conIdEmisor;
    public SimpleStringProperty conIdReceptor;
    List<MensajeEnviadoDto> mensajesEnviados;
    List<MensajeRecibidoDto> mensajesRecibidos;
    private Boolean modificado;
    
    public ConversacionDto() {
        this.modificado = false;
        this.conId = new SimpleStringProperty();
        this.conIdEmisor = new SimpleStringProperty();
        this.conIdReceptor = new SimpleStringProperty();
        mensajesEnviados = FXCollections.observableArrayList();
        mensajesRecibidos = FXCollections.observableArrayList();
    }

    public Long getConId() {
        if(conId.get()!=null && !conId.get().isEmpty())
            return Long.valueOf(conId.get());
        else
            return null;
    }

    public void setConId(Long conId) {
        this.conId.set(conId.toString());
    }

    public Long getConIdEmisor() {
        if(conIdEmisor.get()!=null && !conIdEmisor.get().isEmpty())
            return Long.valueOf(conIdEmisor.get());
        else
            return null;
    }

    public void setConIdEmisor(Long conIdEmisor) {
        this.conIdEmisor.set(conIdEmisor.toString());
    }

    public Long getConIdReceptor() {
        if(conIdReceptor.get()!=null && !conIdReceptor.get().isEmpty())
            return Long.valueOf(conIdReceptor.get());
        else
            return null;
    }

    public void setConIdReceptor(Long conIdReceptor) {
        this.conIdReceptor.set(conIdReceptor.toString());
    }

    public List<MensajeEnviadoDto> getMensajesEnviados() {
        return mensajesEnviados;
    }

    public void setMensajesEnviados(List<MensajeEnviadoDto> mensajesEnviados) {
        this.mensajesEnviados = mensajesEnviados;
    }

    public List<MensajeRecibidoDto> getMensajesRecibidos() {
        return mensajesRecibidos;
    }

    public void setMensajesRecibidos(List<MensajeRecibidoDto> mensajesRecibidos) {
        this.mensajesRecibidos = mensajesRecibidos;
    }

    public Boolean getModificado() {
        return modificado;
    }

    public void setModificado(Boolean modificado) {
        this.modificado = modificado;
    }

    @Override
    public String toString() {
        return "ConversacionesDto{" + "conId=" + conId + ", conIdEmisor=" + conIdEmisor + ", conIdReceptor=" + conIdReceptor + ", mensajesEnviados=" + mensajesEnviados + ", mensajesRecibidos=" + mensajesRecibidos + ", modificado=" + modificado + '}';
    }
    
}
