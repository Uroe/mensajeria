/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.mensajeria.model;

import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author JosueNG
 */
public class MensajeRecibidoDto {

    public SimpleStringProperty id;
    public SimpleStringProperty idReceptor;
    public SimpleStringProperty mensaje;
    public ConversacionDto conversacion;

    private Boolean modificado;

    public MensajeRecibidoDto() {
        this.modificado = false;
        this.id = new SimpleStringProperty();
        this.idReceptor = new SimpleStringProperty();
        this.mensaje = new SimpleStringProperty();
        conversacion = new ConversacionDto();
    }

    public Long getId() {
        if (id.get() != null && !id.get().isEmpty()) {
            return Long.valueOf(id.get());
        } else {
            return null;
        }
    }

    public void setId(Long id) {
        this.id.set(id.toString());
    }

    public Long getIdReceptor() {
        if (idReceptor.get() != null && !idReceptor.get().isEmpty()) {
            return Long.valueOf(idReceptor.get());
        } else {
            return null;
        }
    }

    public void setIdReceptor(Long idReceptor) {
        this.idReceptor.set(idReceptor.toString());
    }

    public String getMensaje() {
        return mensaje.get();
    }

    public void setMensaje(String mensaje) {
        this.mensaje.set(mensaje);
    }

    public Boolean getModificado() {
        return modificado;
    }

    public void setModificado(Boolean modificado) {
        this.modificado = modificado;
    }

    public ConversacionDto getConversacion() {
        return conversacion;
    }

    public void setConversacion(ConversacionDto conversacion) {
        this.conversacion = conversacion;
    }

    @Override
    public String toString() {
        return "MensajesRecibidosDto{" + "id=" + id + ", idReceptor=" + idReceptor + ", mensaje=" + mensaje + '}';
    }

}
